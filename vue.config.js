module.exports = {
  devServer: {
    disableHostCheck: true,
  },
  // add the next line to make the app work on deploy, just add it dont ask me why...
  publicPath: '',

  transpileDependencies: ['vuetify'],

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false,
    },
  },
}
