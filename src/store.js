import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// export default new Vuex.Store({
export default  new Vuex.Store({
  state: {
    barColor: 'rgba(0, 0, 0, .8), rgba(0, 0, 0, .8)',
    barImage: 'https://demos.creative-tim.com/material-dashboard/assets/img/sidebar-1.jpg',
    drawer: null,
    currentUser : sessionStorage.getItem('EOTUSER'),
    reicpesList: [],
  },
  mutations: {
    SET_BAR_IMAGE (state, payload) {
      state.barImage = payload
    },
    SET_DRAWER (state, payload) {
      state.drawer = payload
    },
    SET_DRAWER (state, payload) {
      state.drawer = payload
    },
    
    loginSuccess(state, payload){
      state.isLoggedin = true;
      state.currentUser = Object.assign({}, payload.profile, { token : payload.token });
    },
    
    addReicpe(state, reicpe) {
      state.reicpesList.push(reicpe)
    },
    
    editReicpe(state, reicpe) {
      
      state.reicpesList.forEach((element, index) => {
        if (reicpe.id == element.id){
          state.reicpesList[index] = reicpe 
        }
      });
    },

    deleteReicpe(state, reicpeId){
      state.reicpesList.forEach((element, index) => {
        if (reicpeId == element.id){
          state.reicpesList.splice(index, 1); 
        }
      });
    }

  },
  getters:{
    
  },
  actions: {
    
  },
})
