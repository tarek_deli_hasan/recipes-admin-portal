import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
// import store from './store.js'

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      component: () => import('@/views/CustomComponents/Login'),
    },
    {
      // path: '/login',
      path: '/',
      beforeEnter : guardMyroute,
      // component: () => import('@/views/CustomComponents/Login'),
      component: () => import('@/views/dashboard/Index'),
      children: [
        // Dashboard
        {
          name: 'Dashboard',
          path: '',
          component: () => import('@/views/dashboard/Dashboard'),
        },
        // Pages
        // {
        //   name: 'User Profile',
        //   path: 'pages/user',
        //   component: () => import('@/views/dashboard/pages/UserProfile'),
        // },
        // {
        //   name: 'Notifications',
        //   path: 'components/notifications',
        //   component: () => import('@/views/dashboard/component/Notifications'),
        // },
        // {
        //   name: 'Icons',
        //   path: 'components/icons',
        //   component: () => import('@/views/dashboard/component/Icons'),
        // },
        // {
        //   name: 'Typography',
        //   path: 'components/typography',
        //   component: () => import('@/views/dashboard/component/Typography'),
        // },
        // Tables
        // {
        //   name: 'Regular Tables',
        //   path: 'tables/regular-tables',
        //   component: () => import('@/views/dashboard/tables/RegularTables'),
        // },
        // Maps
        // {
        //   name: 'Google Maps',
        //   path: 'maps/google-maps',
        //   component: () => import('@/views/dashboard/maps/GoogleMaps'),
        // },
        // Upgrade
        // {
        //   name: 'Upgrade',
        //   path: 'upgrade',
        //   component: () => import('@/views/dashboard/Upgrade'),
        // },
        {
          name: 'Reicpes Table',
          path: 'RecipesList',
          component: () => import('@/views/CustomComponents/RecipesList'),
        },
        // {
        //   name: 'Banners Table',
        //   path: 'BannerList',
        //   component: () => import('@/views/CustomComponents/BannerList'),
        // },
        // {
        //   name: 'Products Table',
        //   path: 'ProductsList',
        //   component: () => import('@/views/CustomComponents/ProductsList'),
        // },
        // {
        //   name: 'Countries Table',
        //   path: 'CountriesList',
        //   component: () => import('@/views/CustomComponents/CountriesList'),
        // },
        // {
        //   name: 'Cities Table',
        //   path: 'CitiesList',
        //   component: () => import('@/views/CustomComponents/CitiesList'),
        // },
        // {
        //   name: 'Spare Parts Table',
        //   path: 'spareParts',
        //   component: () => import('@/views/CustomComponents/sparePartList'),
        // },
        // {
        //   name: 'Subscription Table',
        //   path: 'subscription',
        //   component: () => import('@/views/CustomComponents/subscriptionList'),
        // },
        // {
        //   name: 'Diamond Orders Table',
        //   path: 'diamond',
        //   component: () => import('@/views/CustomComponents/diamondList'),
        // },
        // {
        //   name: 'Auctions Table',
        //   path: 'auction',
        //   component: () => import('@/views/CustomComponents/auctionList'),
        // },
        // {
        //   name: 'Auction Users Table',
        //   path: 'auctionUsersList',
        //   component: () => import('@/views/CustomComponents/auctionUsersList'),
        // },
        // {
        //   name: 'Contact & About us',
        //   path: 'contactUs&aboutUs',
        //   component: () => import('@/views/CustomComponents/contactUs&aboutUs'),
        // },
        // {
        //   name: 'Notifications',
        //   path: 'notifications',
        //   component: () => import('@/views/CustomComponents/notifications'),
        // },
        // {
        //   name: 'Subscribers Table',
        //   path: 'subscribersList',
        //   component: () => import('@/views/CustomComponents/subscribersList'),
        // },
        // {
        //   name: 'Subscribers Table',
        //   path: 'systemAdminsList',
        //   component: () => import('@/views/CustomComponents/systemAdminsList'),
        // },
        // {
        //   path: '/login',
        //   component: () => import('@/views/CustomComponents/Login'),
        // },
      ],
    },
  ],
})

function guardMyroute(to, from, next) {
  var isAuthenticated = false;
  let userCreds = JSON.parse(sessionStorage.getItem('recipeUser'))
  let userTempCreds = JSON.parse(sessionStorage.getItem('recipeTempUser'))
  
  if(userCreds){
    if(userCreds.userName == userTempCreds.userName && userCreds.password == userTempCreds.password){
      isAuthenticated = true;
    }
  }
  else
    isAuthenticated = false;
  if(isAuthenticated) 
  {
    next(); // allow to enter route
  } 
  else
  {
    next('/login'); // go to '/login';
  }
}
