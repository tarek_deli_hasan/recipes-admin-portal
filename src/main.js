// =========================================================
// * Vuetify Material Dashboard - v2.1.0
// =========================================================
//
// * Product Page: https://www.creative-tim.com/product/vuetify-material-dashboard
// * Copyright 2019 Creative Tim (https://www.creative-tim.com)
//
// * Coded by Creative Tim
//
// =========================================================
//
// * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import router from './router'
import store from './store'
import './plugins/base'
import './plugins/chartist'
import './plugins/vee-validate'
import vuetify from './plugins/vuetify'
import i18n from './i18n'
import Toasted from 'vue-toasted';


 
Vue.use(VueAxios, axios)
// request BaseUrl
axios.defaults.baseURL = (process.env.API_PATH !== 'production') ? 'http://eot.rawwiki.com/api/' : 'http://eot.rawwiki.com/api/';
//  request interceptor
axios.interceptors.request.use(function (config) {
  // const token = store.getState().session.token;

  // const User = store.getters.currentUser ? (JSON.parse(JSON.stringify(store.getters.currentUser))) : null;
  // let sessionStorage = sessionStorage.getItem('USER');
  let token = null;
  if(sessionStorage.getItem('EOTTOKEN')){
    token = JSON.parse(JSON.stringify(sessionStorage.getItem('EOTTOKEN'))) ;
  }

  config.headers.Authorization = `Bearer ${token}`;
  return config;
});

let ToasterOptions = {
  position : 'bottom-center',
  keepOnHover : true,
  singleton : true,
  duration : 4000
};
Vue.use(Toasted, ToasterOptions)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App),
}).$mount('#app')
